package ru.t1.gorodtsova.tm.configuration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.gorodtsova.tm.api.IPropertyService;

import javax.jms.ConnectionFactory;

@Configuration
@ComponentScan("ru.t1.gorodtsova.tm")
public class LoggerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public ConnectionFactory connectionFactory() {
        return new ActiveMQConnectionFactory(propertyService.getBrokerUrl());
    }

}
