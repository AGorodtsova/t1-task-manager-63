<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h1>PROJECT EDIT</h1>
<form action="/project/edit?id=${project.id}" method="POST">

<input type="hidden" name="id" value="${project.id}"/>

<p>
    <div>NAME:</div>
    <div><input type="text" name="name" value="${project.name}"/></div>
</p>

<p>
    <div>DESCRIPTION:</div>
    <div><input type="text" name="description" value="${project.description}"/></div>
</p>

<p>
    <div>STATUS:</div>
    <select name="status">
        <c:forEach var="status" items="${statuses}">
            <option <c:if test="${project.status == status}">selected="selected"</c:if>
                    value="${status}">${status.displayName}</option>
        </c:forEach>
    </select>
</p>

<p>
    <div>DATE START:</div>
    <div><input type="date" name="dataStart"
                value="<fmt:formatDate pattern="yyyy-MM-dd" value="${project.dateStart}" />"/></div>
</p>

<p>
    <div>DATE FINISH:</div>
    <div><input type="date" name="dataFinish"
                value="<fmt:formatDate pattern="yyyy-MM-dd" value="${project.dateFinish}" />"/></div>
</p>

<button type="submit">SAVE PROJECT</button>

</form>

<jsp:include page="../include/_footer.jsp"/>
