package ru.t1.gorodtsova.tm.servlet;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@WebServlet("/project/edit/*")
public class ProjectEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final Project project = ProjectRepository.getInstance().findById(id);
        req.setAttribute("project", project);
        req.setAttribute("statuses", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(req, resp);
    }

    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final String name = req.getParameter("name");
        @NotNull final String description = req.getParameter("description");
        @NotNull final String statusValue = req.getParameter("status");
        @NotNull final Status status = Status.valueOf(statusValue);
        @NotNull final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        @Nullable final String dateStartValue = req.getParameter("dataStart");
        @Nullable final String dateFinishValue = req.getParameter("dataFinish");

        @NotNull final Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setStatus(status);
        if (!dateStartValue.isEmpty()) project.setDateStart(simpleDateFormat.parse(dateStartValue));
        else project.setDateStart(null);
        if (!dateFinishValue.isEmpty()) project.setDateFinish(simpleDateFormat.parse(dateFinishValue));
        else project.setDateFinish(null);

        ProjectRepository.getInstance().save(project);
        resp.sendRedirect("/projects");
    }

}
