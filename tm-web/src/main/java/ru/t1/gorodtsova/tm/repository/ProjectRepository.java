package ru.t1.gorodtsova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {

    @NotNull
    private static final ProjectRepository INSTANCE = new ProjectRepository();

    @NotNull
    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        for (int i = 1; i < 4; i++) {
            save(new Project("TEST PROJECT " + i, "TEST PROJECT" + i));
        }
    }

    public void create() {
        save(new Project("New project " + System.currentTimeMillis()));
    }

    public void save(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    @Nullable
    public Collection<Project> findAll() {
        return projects.values();
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

}
