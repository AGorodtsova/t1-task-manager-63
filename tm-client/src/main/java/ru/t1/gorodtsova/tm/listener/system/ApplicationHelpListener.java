package ru.t1.gorodtsova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.api.IListener;
import ru.t1.gorodtsova.tm.event.ConsoleEvent;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @NotNull
    private final String ARGUMENT = "-h";

    @NotNull
    private final String DESCRIPTION = "Show command list";

    @NotNull
    private final String NAME = "help";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[HELP]");
        for (@NotNull final IListener listener : listeners) System.out.println(listener);
    }

}
