package ru.t1.gorodtsova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.user.UserUpdateProfileRequest;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.event.ConsoleEvent;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

@Component
public final class UserUpdateProfileListener extends AbstractUserListener {

    @NotNull
    private final String DESCRIPTION = "Update profile of current user";

    @NotNull
    private final String NAME = "update-user-profile";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userUpdateProfileListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken(), firstName, lastName, middleName);
        userEndpoint.updateProfileUser(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
